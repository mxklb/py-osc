FROM mxklb/py-docker:2.0.0
#FROM localhost:5000/py-docker:latest

WORKDIR /app

COPY requirements.txt ./
RUN pip3 install -r requirements.txt && rm requirements.txt